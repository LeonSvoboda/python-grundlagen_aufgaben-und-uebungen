"""
Programmieren Sie ein Programm, in dem der Computer zufällig eine Zahl zwischen 1 bis 10 auswählt. 
Der Nutzer wird anschließend aufgefordert diese Zahl zu eraten. Jedes Mal, wenn der Benutzer falsch rät, erhält er einen Hinweis (zu hoch / zu niedrig), und seine Punktzahl wird reduziert.
Am Ende erhält der Nutzer seinen Score.

Erweiterung:
Der Bereich indem die Zahl vom Computer ausgewählt wird, kann zuvor vom Benutzer gewählt werden (z.B. 1 bis 50, 1-100, etc.)

Die Bandbreite der Hinweise könnte erweitert werden durch: 
* ein Vielfaches 
* ohne Rest teilbar 
* ... etc.
ebenso wäre eine Kombination aus mehreren Hinweisen denkbar

Wenn der Nutzer zweimal die gleiche Zahl eingibt wird sein Score um zwei Punkte reduziert.

"""