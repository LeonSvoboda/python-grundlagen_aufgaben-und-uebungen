""" 
Schreiben Sie ein Programm, das Temperaturen in verschiedene Skalensysteme umwandelt. 
Das Programm soll zu Beginn dem Nutzer eine Auswahl mit den verschiedenen Möglichkeiten anbieten.

Bsp Auswahl: 
(1) Umrechnung von Celsius nach Kelvin
(2) Umrechnung von Celsius nach Fahrenheit
(3) Umrechnung von Kelvin nach Celsius
(4) Umrechnung von Kelvin nach Fahrenheit
(5) Umrechnung von Fahrenheit nach Celsius
(6) Umrechnung von Fahrenheit nach Kelvin

Eine kleine Hilfe:
Celsius = 5/9 * (Fahrenheit - 32).
Celsius = Kelvin - 273.15.
Die tiefste mögliche Temperatur ist der absolute Nullpunkt. => 0K
"""



