""" 
Programmieren Sie einen einfachen Würfelsimulator, der immer wenn er Nutzer das Programm ausführt eine zufällige Zahl von 1-6 in der Konsole anzeigt.

Erweiterung:
Passe dein Programm so an, dass der Benutzer mehrere Würfe nacheinander durchführen kann und den Zeitpunkt zum Beenden des Programms selber bestimmt.
Außerdem soll es möglich sein die Anzahl der Würfelseiten anzupassen.

Bspl:
- 4-seitig mit Ziffen 1-4
- 6-seitig mit Ziffern 1-6
- 8-seitig mit Ziffern 1-8
- 10-seitig mit Ziffern 1-10
- 12-seitig mit Ziffern 1-12
- 20-seitig mit Ziffern 1-20
Bonus: 1x 10-seitig mit Ziffern 10-100

Erweitern Sie das Programm um weitere sinvolle Features. 
"""