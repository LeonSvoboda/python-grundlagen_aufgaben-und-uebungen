""" 
Programmieren Sie ein einfaches Quizspiel, das Fragen an den Nutzer stellt und die Eingaben (Antworten) des Nutzers entsprechend auswertet.

Sie können dabei selbst entscheiden: 
Werden dem Nutzer mehrer Antworten zu Wahl angeboten oder muss er die Antwort korrekt eingeben?
Soll der Nutzer nach der Antwort direkt ein Feedback bekommen oder wird erst am Ende ausgewertet?

Eine Mögliche Erweiterunge ist die Implementation eines Punktesystems für jede richtige Antwort.

"""